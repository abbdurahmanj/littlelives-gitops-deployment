
# LittleLives Continous Delivery Management

  ![LittleLives Infra Resources](./docs/architecture.png)
  

# What does this repository doing ?

## 1. New Service
```mermaid
sequenceDiagram
Engineer ->> GitHub: Push deployment definition
Engineer ->> ArgoCD: Create apps definition
Engineer ->> ArgoCD: Sync Apps
ArgoCD -->> GitHub: Request Pull
GitHub ->> ArgoCD: Repo Pulled
ArgoCD ->> Kubernetes Cluster: Deploy Resources
Kubernetes Cluster -->> ArgoCD: Resources deployed
ArgoCD -->> Engineer: Deployed
```

---

## 2. Update / Upgrade Service
```mermaid
sequenceDiagram
Engineer ->> GitHub: Push repo change
Engineer ->> ArgoCD: Sync Apps
ArgoCD -->> GitHub: Request Pull
GitHub ->> ArgoCD: Repo Pulled
ArgoCD ->> Kubernetes Cluster: Adjust resources
Kubernetes Cluster -->> ArgoCD: resources adjusted
ArgoCD -->> Engineer: Changed
```

---

## 3a. Rollback - ArgoCD
```mermaid
sequenceDiagram
Engineer ->> ArgoCD: History & Set Rollback
Engineer ->> ArgoCD: Refresh Apps
ArgoCD ->> Kubernetes Cluster: Rollback resources
Kubernetes Cluster -->> ArgoCD: resources Rollbacked
ArgoCD -->> Engineer: Rollbacked
```

---

## 3b. Rollback - GitHub
```mermaid
sequenceDiagram
Engineer ->> GitHub: Push revert git
Engineer ->> ArgoCD: Sync Apps
ArgoCD -->> GitHub: Request Pull
GitHub ->> ArgoCD: Repo Pulled
ArgoCD ->> Kubernetes Cluster: Adjust resources
Kubernetes Cluster -->> ArgoCD: resources adjusted
ArgoCD -->> Engineer: Changed
```

**note : this sequence diagram is just showing for this LittleLives golang main apps infrastructure needed, but can extend depends on what resource you use**

<br/>

<br/>

<br/>

  

# How flexible is this repository ?

## 1. New Entity

*Let say LittleLives spread their business into new subsidiary company, but the "infra" stuff still wants to be managed by existing LittleLives team, so here are the simple few steps*

- create new folder on `live/{entity-name}`

- copy all of resource folder from another entity that new entity will use

## 2. New Service

*Let say LittleLives spread their business and have more and more services, but infra team wants the repo keep simple*

- create new folder on `live/{entity-name}/{svc-name}`

- create some yaml resource to define how you want to deploy your apps to a cluster